use anyhow::{Context, Result};
use state_machine::Handle;
use tokio::sync::mpsc;

#[derive(serde::Serialize, serde::Deserialize)]
struct Message {
	text: String,
}

struct SporyServerProtocol;

impl cborpc::Protocol for SporyServerProtocol {
	type State = mpsc::UnboundedSender<String>;
	fn name(&self) -> String {
		"spory-0".into()
	}
	fn method(&mut self, state: &mut Self::State, method_name: &str, message: &[u8]) -> Result<cborpc::CallResponse> {
		match method_name {
			"send-message" => {
				let message: Message = rmp_serde::decode::from_read(message)?;
				state.send(message.text).unwrap();
				Ok(cborpc::CallResponse { success: true, message: vec![] })
			}
			_ => Err(anyhow::anyhow!("No such method")),
		}
	}
}

fn project_dir() -> Result<directories::ProjectDirs> {
	directories::ProjectDirs::from("", "spory", "spory").context("failed to lookup data directory")
}

async fn create_sam_session() -> Result<solitude::Session> {
	let auth_data_dir = project_dir()?.data_dir().join("auth_data");
	let public_key_path = auth_data_dir.join("public_key");
	let private_key_path = auth_data_dir.join("private_key");
	let id: u64 = rand::random();
	let service = format!("spory-{}", id);
	let session_style = solitude::SessionStyle::Stream;

	match std::fs::read(&public_key_path).and_then(|x| Ok((x, std::fs::read(&private_key_path)?))) {
		Ok((public_key_vec, private_key_vec)) => {
			let public_key = String::from_utf8_lossy(&public_key_vec).into();
			let private_key = String::from_utf8_lossy(&private_key_vec).into();
			let session = solitude::Session::from(&service, session_style, &public_key, &private_key).await?;
			Ok(session)
		}
		Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {
			std::fs::create_dir_all(&auth_data_dir)?;
			let session = solitude::Session::new(&service, session_style).await?;
			// std::fs::write(&public_key_path, &session.public_key).context("failed to write public key")?;
			// std::fs::write(&private_key_path, &session.private_key).context("failed to write private key")?;
			Ok(session)
		}
		Err(e) => Err(e).context("error reading SAM session keys"),
	}
}

struct CorrespondentConnection {
	client: cborpc::Client<tokio::io::ReadHalf<tokio::io::BufStream<tokio::net::TcpStream>>, tokio::io::WriteHalf<tokio::io::BufStream<tokio::net::TcpStream>>>,
}

impl CorrespondentConnection {
	async fn new(session: &mut solitude::Session, device_address: &str) -> Result<CorrespondentConnection> {
		let destination = session.look_up(device_address).await?;

		log::info!("Connecting to server");
		let stream = session.connect_stream(destination).await?;

		let (reader, writer) = tokio::io::split(stream);
		let client = cborpc::Client::from_async_stream(reader, writer);
		Ok(CorrespondentConnection { client })
	}

	async fn send_message(&mut self, message: &str) -> Result<()> {
		log::info!("Writing message");
		let msg = Message { text: message.into() };
		let msg_bytes = rmp_serde::encode::to_vec(&msg).unwrap();
		let call = cborpc::MethodCall {
			protocol_name: "spory-0".to_string(),
			method_name: "send-message".to_string(),
			message: msg_bytes,
		};
		let _cr = self.client.call(&call).await?;

		Ok(())
	}
}

#[derive(Default)]
struct Client {
	driver: state_machine::TokioDriver<Client>,
}

impl state_machine::AsyncEventMachine for Client {
	type InEvent = ClientInEvent;
	type OutEvent = ClientOutEvent;
	type Driver = state_machine::TokioDriver<Self>;
}

#[derive(Debug, Clone)]
enum ClientInEvent {
	SendMessage(String, String),
}
#[derive(Debug, Clone)]
enum ClientOutEvent {
	MessageSent(String),
}

impl Client {
	async fn listen(event_channel: mpsc::UnboundedSender<String>) -> Result<()> {
		let mut session = create_sam_session().await?;
		let tcp_listener = tokio::net::TcpListener::bind("127.0.0.1:0").await?;
		let port = tcp_listener.local_addr()?.port();
		log::info!("port: {}", port);

		log::info!("Forwarding tcp server to i2p");
		session.forward("127.0.0.1", port).await?;

		log::info!("Listening on {}", session.address()?);

		loop {
			let (mut stream, _address) = tcp_listener.accept().await.unwrap();
			let listener_event_channel = event_channel.clone();
			tokio::task::spawn(async move {
				let mut stream = tokio::io::BufStream::new(&mut stream);
				let stream_info = solitude::StreamInfo::from_bufread(&mut stream).await.unwrap();
				let (mut reader, mut writer) = tokio::io::split(stream);
				log::info!("connection from {}", stream_info.destination);
				loop {
					let mut responder = cborpc::Responder::new(listener_event_channel.clone());
					responder.add_protocol(SporyServerProtocol);
					while let Some(_response) = cborpc::Responder::answer_call(&mut responder, &mut reader, &mut writer).await.unwrap() {}
				}
			});
		}
	}

	async fn run(mut self) -> Result<()> {
		let (inbox_sender, mut inbox_receiver) = mpsc::unbounded_channel::<String>();
		tokio::task::spawn(Self::listen(inbox_sender));
		loop {
			tokio::select! {
				in_event = self.driver.guts.receiver.recv() => match in_event.unwrap() {
					ClientInEvent::SendMessage(device, message) => {
						eprintln!("sending");
						let handle = self.driver.handle.clone();
						tokio::task::spawn(async move {
							let mut session = create_sam_session().await?;
							let mut cc = CorrespondentConnection::new(&mut session, &device).await?;

							cc.send_message(&message).await?;
							eprintln!("sent");

							handle.broadcast_event(ClientOutEvent::MessageSent(device));
							Result::<()>::Ok(())

						});
					},
				},
				message = inbox_receiver.recv() => {
					eprintln!("Message: {}", message.unwrap());
				},
			}
		}
	}
}

#[derive(clap::Parser)]
enum Command {
	Send { host: String, message: String },
}

#[tokio::main]
async fn main() -> Result<()> {
	env_logger::builder().parse_env("LOG_LEVEL").init();

	let client = Client::default();
	let mut console_controller = state_machine::SimpleTokioMachine::<Client>::default();

	client.driver.handle.add_subscriber(&console_controller.0.handle);
	console_controller.0.handle.add_subscriber(&client.driver.handle);

	let handle = client.driver.handle.clone();
	tokio::task::spawn(async move {
		let res = client.run().await;
		eprintln!("res {:?}", res);
	});
	tokio::task::spawn(async move {
		loop {
			match console_controller.0.guts.receiver.recv().await {
				Some(ClientOutEvent::MessageSent(message)) => {
					eprintln!("Message sent: {message}")
				}
				None => (),
			}
		}
	});
	use clap::Parser;
	use std::io::Write;
	loop {
		print!(">");
		std::io::stdout().flush().unwrap();
		let mut input = String::new();
		let _line = std::io::stdin().read_line(&mut input).unwrap();
		let mut sline = shellwords::split(&input).unwrap();
		sline.insert(0, ".".into());
		match Command::try_parse_from(&sline) {
			Ok(cmd) => match cmd {
				Command::Send { host, message } => {
					eprintln!("Sending {message} to {host}");
					let _ = handle.sender.send(ClientInEvent::SendMessage(host, message)).unwrap();
				}
			},
			Err(err) => err.print().unwrap(),
		}
	}
}
